package pw.binom.date

object TestData {
    /**
     * unix timestamp for 0000-01-01 00:00:00 UTC
     */
    const val ZERO_TIME = -62167219200000L
}