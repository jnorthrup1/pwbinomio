package pw.binom

object Versions {
    val LIB_VERSION = "0.1.31"
    val TEST_CONTAINERS_VERSION = "0.1.1"
    val KOTLIN_VERSION = "1.5.31"
    val KOTLINX_SERIALIZATION_VERSION = "1.3.0"
}